package com.heavensbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeavensBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(HeavensBankApplication.class, args);
	}

}
