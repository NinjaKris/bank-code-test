package com.heavensbank.controller;

import com.heavensbank.entity.User;
import com.heavensbank.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.inject.Inject;
import javax.validation.Valid;


@Slf4j
@RestController
@RequestMapping(value = "/user/registration")
public class LoginController {

    @Inject
    UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showRegistrationForm() {
        return new ModelAndView("register", "user", new User());
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView registerUserAccount
            ( @Valid @ModelAttribute("username") String username,
              @Valid @ModelAttribute("password") String password,
              BindingResult result) {
        User user = new User(username, password);
        try {
            userService.registerNewUserAccount(user);
        } catch (Exception e) {
            System.out.println(e.getCause());
            return new ModelAndView("register");
        }
        return new ModelAndView("main");
    }
}
