package com.heavensbank.port;

import com.heavensbank.entity.User;

public interface UserServicePort {
    User registerNewUserAccount(User accountDto)
            throws Exception;
}
