package com.heavensbank.repository;

import com.heavensbank.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface UserRepo extends CrudRepository<User, UUID>, JpaRepository<User, UUID> {
        User findByUserName(String name);
}
