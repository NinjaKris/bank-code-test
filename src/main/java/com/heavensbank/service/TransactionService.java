package com.heavensbank.service;

import com.heavensbank.entity.Account;
import com.heavensbank.repository.AccountRepo;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

import javax.inject.Inject;
import java.util.List;

@Service
public class TransactionService {

    @Inject
    AccountRepo accountRepo;

    //TODO Error handling

    public Account deposit(Account account){
        return accountRepo.save(account);
    }
    public Account withdraw(Account account){
        Long negativeAmount = account.getAmount()*-1;
        account.setAmount(negativeAmount);
        return accountRepo.save(account);
    }

    public List<Account> getAccounts(String userId){
        return accountRepo.findAllByUserId(userId);
    }

    public long getBalance(){
        List<Account> transactions = accountRepo.findAllByUserId(RequestContextHolder.currentRequestAttributes().getSessionId());

       return transactions.stream()
                .mapToLong(u -> u.getAmount()).sum();
    }
}
