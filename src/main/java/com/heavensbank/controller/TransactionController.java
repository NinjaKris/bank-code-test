package com.heavensbank.controller;

import com.heavensbank.entity.Account;
import com.heavensbank.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.MediaTypes;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import javax.inject.Inject;
import javax.validation.Valid;


import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@RequestMapping(value = "/main", produces = {APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE})
public class TransactionController {

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showMainForm() {
        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();
        List<Account> accounts = transactionService.getAccounts(sessionId);
        ModelAndView modelAndView = new ModelAndView("main");
        modelAndView.addObject("accounts",accounts);
        modelAndView.addObject("balance",transactionService.getBalance());
        return modelAndView;
    }

    //TODO Rename to Transaction
    @Inject
    TransactionService transactionService;

    @RequestMapping(value = "/deposit", method = RequestMethod.POST)
    public ModelAndView deposit(@Valid @ModelAttribute("amount") long amount
            , BindingResult result) {
        if(result.hasErrors()){
            // Throw an exception .
        }
        Account account = new Account();
        account.setType("deposit");
        account.setUserId(RequestContextHolder.currentRequestAttributes().getSessionId());
        account.setAmount(amount);

        transactionService.deposit(account);

        ModelAndView modelAndView = new ModelAndView("redirect:");

        return modelAndView;
    }

    @RequestMapping(value = "/withdraw",
            method= RequestMethod.POST)
    public ModelAndView withdraw(@Valid @ModelAttribute("amount") long amount
            , BindingResult result){

        if(result.hasErrors()){
            // Throw an exception .
        }
        Account account = new Account();
        account.setType("withdraw");
        account.setUserId(RequestContextHolder.currentRequestAttributes().getSessionId());
        account.setAmount(amount);

        transactionService.withdraw(account);

        ModelAndView modelAndView = new ModelAndView("redirect:");

        return modelAndView;
    }
}
