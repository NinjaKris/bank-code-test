package com.heavensbank.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Wither;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;


@Entity
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Wither
@Table(name = "account")
public class Account {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    @JsonProperty
    private UUID id;

    @Column(name = "user_id")
    @JsonProperty
    @NotNull
    private String userId;

    @Column(name = "transaction_type")
    @Getter
    @Setter
    @NotNull
    @NotBlank
    @JsonProperty
    private String type;

    @Column(name = "amount")
    @JsonProperty
    @NotNull
    private Long amount;

    public void setType(String type) {
        this.type = type;
    }
    public String getType() { return this.type; }
    public void setUserId(String id){ this.userId = id; }
    public void setAmount(Long amount){this.amount = amount;}
    public Long getAmount() { return this.amount; }
}