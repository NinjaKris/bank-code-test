package com.heavensbank.service;

import com.heavensbank.entity.User;
import com.heavensbank.port.UserServicePort;
import com.heavensbank.repository.UserRepo;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

import javax.inject.Inject;
import javax.transaction.Transactional;

@Service
public class UserService implements UserServicePort {

    @Inject
    UserRepo userRepo;

    @Transactional
    @Override
    public User registerNewUserAccount(User account)
            throws Exception {

        if (userNameExist(account.getUserName())) {
            throw new Exception(
                    "There is an account with that username: "
                            +  account.getUserName());
        }else {
            account.setId(RequestContextHolder.currentRequestAttributes().getSessionId());
            return userRepo.save(account);
        }
    }

    private boolean userNameExist(String username) {
        User user = userRepo.findByUserName(username);
        if (user != null) {
            return true;
        }
        return false;
    }


}
