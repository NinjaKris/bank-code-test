package com.heavensbank.repository;

import com.heavensbank.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface AccountRepo extends CrudRepository<Account, UUID>, JpaRepository<Account, UUID> {
    List<Account> findAllByUserId(String userId);
}
