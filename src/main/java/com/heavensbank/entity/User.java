package com.heavensbank.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Wither;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Wither
@Table(name = "User")
public class User {

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id")
    @JsonProperty
    @NotNull
    private String id;

    @Column(name = "user_name")
    @JsonProperty
    @NotNull
    @NotEmpty
    private String userName;

    @Column(name = "password")
    @JsonProperty
    @NotNull
    @NotEmpty
    private String password;

    public User() {
    }

    public User(String username, String password) {
        this.userName = username;
        this.password = password;
    }

    public String getUserName(){
        return this.userName;
    }

    public String getPassword(){
        return this.password;
    }

    public void setId(String id){
        this.id = id;
    }

    public void setPassword(String password){
        this.password = password;
    }
}
